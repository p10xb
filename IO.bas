Attribute VB_Name = "IO"
Option Explicit

'Este modulo fu� programado por Vlad - TodoSV.com & SVCommunity.org
Public Function IO_Titulo_de_Archivo(ByVal Ruta_Completa As String) As String
        '<EhHeader>
        On Error GoTo IO_Titulo_de_Archivo_Err
        '</EhHeader>
        Dim i As Long
100     i = InStrRev(Ruta_Completa, "\")
101     IO_Titulo_de_Archivo = Mid$(Ruta_Completa, i)
        '<EhFooter>
        Exit Function
IO_Titulo_de_Archivo_Err:
        Controlar_Error Erl, Err.Description, "P10XB.IO.IO_Titulo_de_Archivo.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Function

Public Function IO_Extension_de_Archivo(ByVal Ruta_Completa_o_Titulo As String) As String
        '<EhHeader>
        On Error GoTo IO_Extension_de_Archivo_Err
        '</EhHeader>
        Dim i As Long
100     i = InStrRev(Ruta_Completa_o_Titulo, ".")

101     If i = 0 Then Exit Function
102     IO_Extension_de_Archivo = Mid$(Ruta_Completa_o_Titulo, i + 1)
        '<EhFooter>
        Exit Function
IO_Extension_de_Archivo_Err:
        Controlar_Error Erl, Err.Description, "P10XB.IO.IO_Extension_de_Archivo.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Function
