VERSION 5.00
Begin VB.Form frmPrincipal 
   Caption         =   "P10XB 1.0 Beta 4"
   ClientHeight    =   7230
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11175
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7230
   ScaleWidth      =   11175
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkCoincidencia 
      Caption         =   "Mostrar coincidencia"
      Height          =   270
      Left            =   5475
      TabIndex        =   7
      Top             =   480
      Width           =   1800
   End
   Begin VB.CheckBox chkEjec 
      Caption         =   "Ofrecer ejecución"
      Height          =   270
      Left            =   7560
      TabIndex        =   6
      Top             =   480
      Width           =   1620
   End
   Begin VB.CommandButton cmdReabrir 
      Caption         =   "Re-Abrir"
      Enabled         =   0   'False
      Height          =   360
      Left            =   9885
      TabIndex        =   5
      Top             =   450
      Width           =   1215
   End
   Begin VB.CheckBox chkASC 
      Caption         =   "Agregar muestra en ASCII"
      Height          =   270
      Left            =   75
      TabIndex        =   4
      Top             =   480
      Width           =   2640
   End
   Begin VB.CheckBox chkHex 
      Caption         =   "Agregar muestra en HEX"
      Height          =   270
      Left            =   2745
      TabIndex        =   3
      Top             =   480
      Width           =   2640
   End
   Begin VB.TextBox txtInfo 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6315
      Left            =   75
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   840
      Width           =   11040
   End
   Begin VB.TextBox txtArchivo 
      Height          =   360
      Left            =   75
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   45
      Width           =   9750
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "Abrir"
      Height          =   360
      Left            =   9900
      TabIndex        =   0
      Top             =   45
      Width           =   1215
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkASC_Click()
        '<EhHeader>
        On Error GoTo chkASC_Click_Err
        '</EhHeader>
100     Mostrar_AscDump = -chkASC.Value
        '<EhFooter>
        Exit Sub
chkASC_Click_Err:
        Controlar_Error Erl, Err.Description, "P10XB.frmPrincipal.chkASC_Click.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Sub

Private Sub chkCoincidencia_Click()
        '<EhHeader>
        On Error GoTo chkCoincidencia_Click_Err
        '</EhHeader>
100     Mostrar_Coincidencia = -chkCoincidencia.Value
        '<EhFooter>
        Exit Sub
chkCoincidencia_Click_Err:
        Controlar_Error Erl, Err.Description, "P10XB.frmPrincipal.chkCoincidencia_Click.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Sub

Private Sub chkEjec_Click()
        '<EhHeader>
        On Error GoTo chkEjec_Click_Err
        '</EhHeader>
100     Ofrecer_Ejecucion = -chkEjec.Value
        '<EhFooter>
        Exit Sub
chkEjec_Click_Err:
        Controlar_Error Erl, Err.Description, "P10XB.frmPrincipal.chkEjec_Click.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Sub

Private Sub chkHex_Click()
        '<EhHeader>
        On Error GoTo chkHex_Click_Err
        '</EhHeader>
100     Mostrar_HexDump = -chkHex.Value
        '<EhFooter>
        Exit Sub
chkHex_Click_Err:
        Controlar_Error Erl, Err.Description, "P10XB.frmPrincipal.chkHex_Click.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Sub

Private Sub cmdBuscar_Click()
        '<EhHeader>
        On Error GoTo cmdBuscar_Click_Err
        '</EhHeader>
        Dim pDlg As New cCommonDialog
        Dim Texto As String
100     If pDlg.VBGetOpenFileName(Archivo, "Seleccione el archivo a procesar", True, False, False, False, , , , "P10XB :: Seleccione su archivo", , hwnd) Then
101         txtArchivo.Text = Archivo
102         sIdentificar = Identificar(Archivo)
103         txtInfo.Text = Escapar_TextBox(sIdentificar.Salida)
104         cmdReabrir.Enabled = True
        End If

        '<EhFooter>
        Exit Sub
cmdBuscar_Click_Err:
        Controlar_Error Erl, Err.Description, "P10XB.frmPrincipal.cmdBuscar_Click.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Sub

Private Sub cmdReabrir_Click()
        '<EhHeader>
        On Error GoTo cmdReabrir_Click_Err
        '</EhHeader>
100     txtArchivo.Text = Archivo
101     sIdentificar = Identificar(Archivo)
102     txtInfo.Text = Escapar_TextBox(sIdentificar.Salida)
        '<EhFooter>
        Exit Sub
cmdReabrir_Click_Err:
        Controlar_Error Erl, Err.Description, "P10XB.frmPrincipal.cmdReabrir_Click.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Sub
