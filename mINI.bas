Attribute VB_Name = "mINI"
Option Explicit
'Este modulo fu� programado por Vladimir - TodoSV.com & SVCommunity.org
Private Declare Function GetPrivateProfileSectionNames _
                Lib "kernel32.dll" _
                Alias "GetPrivateProfileSectionNamesA" (ByVal lpszReturnBuffer As String, _
                                                        ByVal nSize As Long, _
                                                        ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileString _
                Lib "kernel32" _
                Alias "GetPrivateProfileStringA" (ByVal lpSectionName As String, _
                                                  ByVal lpKeyName As Any, _
                                                  ByVal lpDefault As String, _
                                                  ByVal lpbuffurnedString As String, _
                                                  ByVal nBuffSize As Long, _
                                                  ByVal lpFileName As String) As Long

Public Function mINI_Obtener_Nombre_Secciones(Ruta_INI As String) As Collection
        'Lista
        '<EhHeader>
        On Error GoTo mINI_Obtener_Nombre_Secciones_Err
        '</EhHeader>
        Dim Secciones() As String
        Dim szBuf As String, lLen As Integer
        'Obtenemos los nombres de todas las secciones, vienen separadas por NullChar Chr(0)
100     szBuf = String$(32767, vbNullChar) 'Creamos el buffer
101     lLen = GetPrivateProfileSectionNames(szBuf, Len(szBuf), Ruta_INI) 'Obtenemos el largo del la lectura
102     szBuf = Left$(szBuf, lLen) 'Cortamos lo innecesario

103     If szBuf = vbNullString Then
            Exit Function
        End If

104     Secciones = Split(szBuf, vbNullChar)
        Dim i As Long
        Dim a As Long
105     a = UBound(Secciones) - 1
106     Set mINI_Obtener_Nombre_Secciones = New Collection

107     For i = 0 To a
108         mINI_Obtener_Nombre_Secciones.Add Secciones(i)
        Next

        '<EhFooter>
        Exit Function
mINI_Obtener_Nombre_Secciones_Err:
        Controlar_Error Erl, Err.Description, "P10XB.mINI.mINI_Obtener_Nombre_Secciones.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Function

Public Function mINI_Leer_Clave_de_Seccion(Ruta_INI As String, _
                                           nombre_Seccion As String, _
                                           nombre_Clave As String, _
                                           Optional Valor_si_no_existe As String = vbNullString) As String
        '<EhHeader>
        On Error GoTo mINI_Leer_Clave_de_Seccion_Err
        '</EhHeader>
        Dim Buffer As String * 32767
        Dim Lgt As Long
100     Buffer = String$(32767, vbNullChar)
101     Lgt = GetPrivateProfileString(nombre_Seccion, nombre_Clave, Valor_si_no_existe, Buffer, Len(Buffer), Ruta_INI)

102     If Lgt Then mINI_Leer_Clave_de_Seccion = Left$(Buffer, Lgt) Else mINI_Leer_Clave_de_Seccion = vbNullString
        '<EhFooter>
        Exit Function
mINI_Leer_Clave_de_Seccion_Err:
        Controlar_Error Erl, Err.Description, "P10XB.mINI.mINI_Leer_Clave_de_Seccion.Ref 7/6/2008 : 11:57:58"
        Resume Next
        '</EhFooter>
End Function
