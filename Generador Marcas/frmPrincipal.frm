VERSION 5.00
Begin VB.Form frmMarca 
   Caption         =   "Generador de Marcas para P10XB"
   ClientHeight    =   5550
   ClientLeft      =   60
   ClientTop       =   420
   ClientWidth     =   5865
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5550
   ScaleWidth      =   5865
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtProgramaAsociado 
      Height          =   285
      Left            =   45
      TabIndex        =   7
      ToolTipText     =   "Direcci�n web del programa que puede abrir el archivo"
      Top             =   2055
      Width           =   5685
   End
   Begin VB.TextBox txtdirWeb 
      Height          =   285
      Left            =   45
      TabIndex        =   6
      ToolTipText     =   "Direcci�n Web de referencia para el tipo de archivo"
      Top             =   1740
      Width           =   5685
   End
   Begin VB.TextBox txtPluginAsociado 
      Height          =   285
      Left            =   45
      TabIndex        =   5
      ToolTipText     =   "Plugin � programa Asociado"
      Top             =   1425
      Width           =   5685
   End
   Begin VB.TextBox txtExtension 
      Height          =   285
      Left            =   45
      TabIndex        =   4
      ToolTipText     =   "Extensi�n"
      Top             =   1110
      Width           =   5685
   End
   Begin VB.TextBox txtdesLarga 
      Height          =   285
      Left            =   45
      TabIndex        =   3
      ToolTipText     =   "Descripci�n Larga"
      Top             =   795
      Width           =   5685
   End
   Begin VB.TextBox txtdesCorta 
      Height          =   285
      Left            =   45
      TabIndex        =   2
      ToolTipText     =   "Descripci�n Corta"
      Top             =   480
      Width           =   5685
   End
   Begin VB.TextBox txtHex 
      Height          =   285
      Left            =   3090
      TabIndex        =   1
      ToolTipText     =   "Descripci�n HEX (Soporta RegEx)"
      Top             =   120
      Width           =   2640
   End
   Begin VB.TextBox txtASCII 
      Height          =   285
      Left            =   45
      TabIndex        =   0
      ToolTipText     =   "Descripci�n ASCII (Sin RegEx)"
      Top             =   120
      Width           =   2640
   End
   Begin VB.TextBox txtResultado 
      Height          =   3060
      Left            =   30
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   8
      Top             =   2385
      Width           =   5715
   End
   Begin VB.Line Line1 
      X1              =   3030
      X2              =   2730
      Y1              =   270
      Y2              =   270
   End
End
Attribute VB_Name = "frmMarca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type tMarca
    tMarca_Marca As String
    tMarca_desCorta As String
    tMarca_desLarga As String
    tMarca_Extension As String
    tMarca_PluginAsociado As String
    tMarca_dirWeb As String
    tMarca_ProgramaAsociado As String
End Type
Private Marca As tMarca


Private Sub txtASCII_Change()
Dim caracteres As String
Dim i As Integer
Dim buff As String
caracteres = txtASCII.Text
txtHex.Text = ""
For i = 1 To Len(caracteres)
    buff = buff & Right$("00" & Hex(Asc(Mid(caracteres, i, 1))), 2)
Next
txtHex.Text = buff
End Sub

Private Sub txtdesCorta_Change()
Marca.tMarca_desCorta = txtdesCorta.Text
Generar_Marca
End Sub

Private Sub txtdesLarga_Change()
Marca.tMarca_desLarga = txtdesLarga.Text
Generar_Marca
End Sub

Private Sub txtdirWeb_Change()
Marca.tMarca_dirWeb = txtdirWeb.Text
Generar_Marca
End Sub

Private Sub txtExtension_Change()
Marca.tMarca_Extension = txtExtension.Text
Generar_Marca
End Sub

Private Sub txtHex_Change()
Marca.tMarca_Marca = txtHex.Text
Generar_Marca
End Sub

Private Sub txtPluginAsociado_Change()
Marca.tMarca_PluginAsociado = txtPluginAsociado.Text
Generar_Marca
End Sub

Private Sub txtProgramaAsociado_Change()
Marca.tMarca_ProgramaAsociado = txtProgramaAsociado.Text
Generar_Marca
End Sub

Private Sub Generar_Marca()
Dim buff As String
buff = "[" & Marca.tMarca_Marca & "]" & vbNewLine
buff = buff & "desCorta=" & Marca.tMarca_desCorta & vbNewLine
buff = buff & "desLarga=" & Marca.tMarca_desLarga & vbNewLine
buff = buff & "Extension=" & Marca.tMarca_Extension & vbNewLine
buff = buff & "PluginAsociado=" & Marca.tMarca_PluginAsociado & vbNewLine
buff = buff & "dirWeb=" & Marca.tMarca_dirWeb & vbNewLine
buff = buff & "ProgamaAsociado=" & Marca.tMarca_ProgramaAsociado & vbNewLine
txtResultado.Text = buff
End Sub
