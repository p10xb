Attribute VB_Name = "Principal"
'Programado por Vlad para SVCommunity.org y TodoSV.com
'Este c�digo no tiene licencia que lo proteja, uselo a su voluntad.
Option Explicit
Private Declare Function ShellExecute _
                Lib "shell32.dll" _
                Alias "ShellExecuteA" (ByVal hwnd As Long, _
                                       ByVal lpOperation As String, _
                                       ByVal lpFile As String, _
                                       ByVal lpParameters As String, _
                                       ByVal lpDirectory As String, _
                                       ByVal nShowCmd As Long) As Long
Private Type tipo_Marcas
    desCorta As String
    desLarga As String
    Extension As String
    PluginAsociado As String
    dirWeb As String
    ProgamaAsociado As String
End Type
Private Const iden_desCorta As String = "desCorta"
Private Const iden_desLarga As String = "desLarga"
Private Const iden_Extension As String = "Extension"
Private Const iden_PluginAsociado As String = "PluginAsociado"
Private Const iden_dirWeb As String = "dirWeb"
Private Const iden_ProgamaAsociado As String = "ProgamaAsociado"
Private buff_Marcas As New Collection
Private buff_MarcasEx As tipo_Marcas
Private Ruta_INI As String
Private Bandera_Marca_Encontrada As Boolean
Public Type tIdentificar
    sArchivo As String
    Salida As String
End Type
Public sIdentificar As tIdentificar
'Soporte Archivo
Public Archivo As String
'Soporte RegEx
Public objRegExp As New RegExp
'Control de salida
Public Mostrar_HexDump As Boolean
Public Mostrar_AscDump As Boolean
Public Ofrecer_Ejecucion As Boolean
Public Mostrar_Coincidencia As Boolean

Public Sub Main()
        '<EhHeader>
        On Error GoTo Main_Err
        '</EhHeader>
        Dim Canal As Byte
100     Ruta_INI = App.Path & "\marcas.ini"
101     Set buff_Marcas = mINI_Obtener_Nombre_Secciones(Ruta_INI)

102     If Len(Command$) = 0 Then
103         frmPrincipal.Show
            Exit Sub
        End If

104     Archivo = Command$
105     Archivo = Replace$(Archivo, """", "")
107     sIdentificar = Identificar(Archivo)
108     Canal = FreeFile
109     Open App.Path & IO_Titulo_de_Archivo(sIdentificar.sArchivo) & ".resultado.txt" For Binary Access Write As #Canal
110     Put #Canal, , sIdentificar.Salida
111     Close Canal
        '<EhFooter>
        Exit Sub

Main_Err:
        Controlar_Error Erl, Err.Description, "P10XB.Principal.Main.Ref 7/6/2008 : 13:03:02"
        Resume Next
        '</EhFooter>
End Sub

Private Function Convertir_Caracteres(ByVal Texto As String) As String
        '<EhHeader>
        On Error GoTo Convertir_Caracteres_Err
        '</EhHeader>
        Dim i As Integer
        Dim f As Integer
100     f = Len(Texto)

101     For i = 1 To f
102         Convertir_Caracteres = Convertir_Caracteres & Right$("00" & Hex$(Asc(Mid$(Texto, i, 1))), 2)
        Next

        '<EhFooter>
        Exit Function
Convertir_Caracteres_Err:
        Controlar_Error Erl, Err.Description, "P10XB.Principal.Convertir_Caracteres.Ref 7/6/2008 : 11:57:57"
        Resume Next
        '</EhFooter>
End Function

Public Sub Controlar_Error(ByRef nErl As Long, _
                           ByRef Descripcion As String, _
                           Donde As String)
    'CSEH: Skip
    Dim X_Err As Byte
2   X_Err = MsgBox("El siguiente error se produjo: " & vbNewLine & Descripcion & vbNewLine & "En el m�dulo: " & Donde & ", en la linea " & nErl & vbCrLf & "El programa puede estar inestable, sin embargo puede continuar, �desea hacerlo? ([SI] contin�a, [NO] termina el programa)", vbYesNo + vbCritical)
    On Error Resume Next

5   Select Case X_Err

        Case Is = vbYes

6       Case Is = vbNo
8           End
    End Select

End Sub

Public Function Comparar_Marca(ByVal Texto As String) As String
        '<EhHeader>
        On Error GoTo Comparar_Marca_Err
        '</EhHeader>
        Dim i As Long
        Dim Resultado As String

100     For i = 1 To buff_Marcas.Count
101         Resultado = RegEx(buff_Marcas.Item(i), Texto)

102         If Resultado <> "" Then
103             Debug.Print Resultado
104             Cargar_datos_marcas i
105             Comparar_Marca = "Coincide con la marca de un archivo del tipo:" & vbNewLine & buff_MarcasEx.desCorta & " (" & buff_MarcasEx.Extension & ")" & vbNewLine & vbNewLine & "Se describe como:" & vbNewLine & buff_MarcasEx.desLarga & vbNewLine & vbNewLine & "Puedes encontrar m�s informaci�n en esta ubicaci�n: " & buff_MarcasEx.dirWeb & vbNewLine & vbNewLine & "Puedes usar este (estos) programa(s) para abrir este tipo de archivo: " & vbNewLine & Replace$(buff_MarcasEx.ProgamaAsociado, "|", vbNewLine)

106             If Mostrar_Coincidencia Then Comparar_Marca = Comparar_Marca & vbNewLine & vbNewLine & "Detectado por: " & vbNewLine & Resultado
107             Bandera_Marca_Encontrada = True
                Exit Function
            End If

        Next

108     Bandera_Marca_Encontrada = False
109     buff_MarcasEx.desCorta = ""
110     buff_MarcasEx.desLarga = ""
111     buff_MarcasEx.dirWeb = ""
112     buff_MarcasEx.Extension = ""
113     buff_MarcasEx.PluginAsociado = ""
114     buff_MarcasEx.ProgamaAsociado = ""
115     Comparar_Marca = "Marca desconocida, por favor notifique a los creadores de este programa sobre este archivo."
        '<EhFooter>
        Exit Function
Comparar_Marca_Err:
        Controlar_Error Erl, Err.Description, "P10XB.Principal.Comparar_Marca.Ref 7/6/2008 : 11:57:57"
        Resume Next
        '</EhFooter>
End Function

Public Sub Cargar_datos_marcas(Posicion_en_Buff_Marcas As Long)
        '<EhHeader>
        On Error GoTo Cargar_datos_marcas_Err
        '</EhHeader>
100     buff_MarcasEx.desCorta = mINI_Leer_Clave_de_Seccion(Ruta_INI, buff_Marcas.Item(Posicion_en_Buff_Marcas), iden_desCorta)
101     buff_MarcasEx.desLarga = mINI_Leer_Clave_de_Seccion(Ruta_INI, buff_Marcas.Item(Posicion_en_Buff_Marcas), iden_desLarga)
102     buff_MarcasEx.dirWeb = mINI_Leer_Clave_de_Seccion(Ruta_INI, buff_Marcas.Item(Posicion_en_Buff_Marcas), iden_dirWeb)
103     buff_MarcasEx.Extension = mINI_Leer_Clave_de_Seccion(Ruta_INI, buff_Marcas.Item(Posicion_en_Buff_Marcas), iden_Extension)
104     buff_MarcasEx.PluginAsociado = mINI_Leer_Clave_de_Seccion(Ruta_INI, buff_Marcas.Item(Posicion_en_Buff_Marcas), iden_PluginAsociado)
105     buff_MarcasEx.ProgamaAsociado = mINI_Leer_Clave_de_Seccion(Ruta_INI, buff_Marcas.Item(Posicion_en_Buff_Marcas), iden_ProgamaAsociado)
        '<EhFooter>
        Exit Sub
Cargar_datos_marcas_Err:
        Controlar_Error Erl, Err.Description, "P10XB.Principal.Cargar_datos_marcas.Ref 7/6/2008 : 11:57:57"
        Resume Next
        '</EhFooter>
End Sub

Public Function Identificar(Archivo As String) As tIdentificar
        '<EhHeader>
        On Error GoTo Identificar_Err
        '</EhHeader>
        Dim Salida As String
        Dim Entrada As String
        Dim Marca As String
        Dim Canal As Byte

        ' Verificar si existe el archivo.
100     If Len(Dir$(Archivo)) = 0 Or Len(Archivo) = 0 Then
101         Salida = "Error: El archivo no pudo abrirse: " & Archivo
102         Archivo = "\error " & Hour(Time) & "." & Minute(Time) & ".txt"
        Else

            ' Verificar si es un directorio.
103         If GetAttr(Archivo) = vbDirectory Then
104             Salida = "Error: Era un directio: " & Archivo
            Else
                ' Ok es un archivo y existe.
105             Canal = FreeFile

106             If FileLen(Archivo) < 10000 Then
107                 Entrada = Space$(FileLen(Archivo))
                Else
108                 Entrada = Space$(1000)
                End If

109             Open Archivo$ For Binary Access Read As #Canal
110             Get #Canal, , Entrada
111             Close Canal
                'Convertir los 1000 bytes a HEX
112             Marca = Convertir_Caracteres(Entrada)
                'Comparar los 1000 bytes con todas las marcas.
113             Salida = Comparar_Marca(Marca) & vbNewLine & vbNewLine

114             If Mostrar_AscDump Then Salida = Salida & "Muestra ASCII del archivo: " & vbNewLine & Entrada & vbNewLine & vbNewLine
115             If Mostrar_HexDump Then Salida = Salida & "Muestra HEX del archivo: " & vbNewLine & Marca & vbNewLine & vbNewLine & vbNewLine
116             Salida = Salida & "Archivo generado por P10XB @ SvCommunity.org & TodoSV.com"

117             If Bandera_Marca_Encontrada = True Then
                    Dim Extensiones() As String

118                 If RegEx(LCase(IO_Extension_de_Archivo(Archivo)), LCase(buff_MarcasEx.Extension)) = "" Then
119                     If MsgBox("La extensi�n del archivo procesado es """ & IO_Extension_de_Archivo(Archivo) & """, no coincide con la que P10XB cree que es la correcta """ & buff_MarcasEx.Extension & """" & vbNewLine & "�Quisiera que P10XB cambiara la extensi�n del archivo?", vbQuestion + vbYesNo, "Solicito acci�n del usuario") = vbYes Then
120                         Name Archivo As Archivo & "." & buff_MarcasEx.Extension
121                         Archivo = Archivo & "." & buff_MarcasEx.Extension
                        End If
                    End If

                    'Soporte de Plugins
                    'Verificamos que el plugin exista:
122                 If Len(buff_MarcasEx.PluginAsociado) <> 0 Then
                        Dim Plugin_Nombre
                        Dim Plugin_Parametros
123                     Plugin_Nombre = Trim$(Replace(Left$(buff_MarcasEx.PluginAsociado, InStr(1, buff_MarcasEx.PluginAsociado, "$") - 1), ".\", App.Path & "\"))
124                     Plugin_Parametros = Mid$(buff_MarcasEx.PluginAsociado, InStr(1, buff_MarcasEx.PluginAsociado, "$"))
125                     Plugin_Parametros = Replace$(Plugin_Parametros, "$1", """" & Archivo & """")

126                     If Ofrecer_Ejecucion Then
127                         If MsgBox("P10XB cree conocer el programa con el que el archivo puede abrirse. " & vbNewLine & "Se ejecutar� el siguiente comando para intentarlo: " & vbNewLine & Plugin_Nombre & " " & Plugin_Parametros & vbNewLine & "�Esta de acuerdo?", vbYesNo + vbQuestion) = vbYes Then
128                             If Len(buff_MarcasEx.PluginAsociado) <> 0 Then ShellExecute 0, "Open", Plugin_Nombre, Plugin_Parametros, 0, 1
                            End If
                        End If
                    End If
                End If
            End If
        End If

129     Identificar.Salida = Salida
130     Identificar.sArchivo = Archivo
        '<EhFooter>
        Exit Function
Identificar_Err:
        Controlar_Error Erl, Err.Description, "P10XB.Principal.Identificar.Ref 7/6/2008 : 12:11:59"
        Resume Next
        '</EhFooter>
End Function

Public Function Escapar_TextBox(ByRef Texto As String) As String
        'Escapar los NULL
        '<EhHeader>
        On Error GoTo Escapar_TextBox_Err
        '</EhHeader>
100     Texto = Replace$(Texto, Chr(0), "[0]")
101     Escapar_TextBox = Texto
        '<EhFooter>
        Exit Function
Escapar_TextBox_Err:
        Controlar_Error Erl, Err.Description, "P10XB.Principal.Escapar_TextBox.Ref 7/6/2008 : 11:57:57"
        Resume Next
        '</EhFooter>
End Function

Function RegEx(myPattern As String, myString As String)
        '<EhHeader>
        On Error GoTo RegEx_Err
        '</EhHeader>
        Dim RetStr As String
        Dim objMatch As Match
        Dim colMatches As MatchCollection
        ' Set Case Insensitivity.
100     objRegExp.IgnoreCase = True
        'Set global applicability.
101     objRegExp.Global = True
        'Set the pattern by using the Pattern property.
102     objRegExp.Pattern = myPattern

        'Test whether the String can be compared.
103     If (objRegExp.Test(myString) = True) Then
            'Get the matches.
104         Set colMatches = objRegExp.Execute(myString)   ' Execute search.

105         For Each objMatch In colMatches   ' Iterate Matches collection.
106             RetStr = RetStr & "Coincidencia encontrada en pos. "
107             RetStr = RetStr & objMatch.FirstIndex & ". Valor de la coincidencia es "
108             RetStr = RetStr & objMatch.Value & vbCrLf
            Next

        Else
109         RetStr = ""
        End If

110     RegEx = RetStr
        '<EhFooter>
        Exit Function
RegEx_Err:
        Controlar_Error Erl, Err.Description, "P10XB.Principal.RegEx.Ref 7/6/2008 : 11:57:57"
        Resume Next
        '</EhFooter>
End Function
